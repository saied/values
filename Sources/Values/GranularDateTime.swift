import Foundation

public class GranularDateTime: Codable {
    var datetime: String?
    var granularity: String?

    let formatIn = "E, d MMM yyyy HH:mm:ss Z"

    let formatOut = "E, d MMM yyyy"

    let locale = Locale(identifier: "en_GB_POSIX")

    public init() {}

    public func getDate() -> String? {
        return datetime
    }

    @discardableResult public func setDate(value: String?) -> Self {
        datetime = value
        return self
    }

    public func hasDate() -> Bool {
        return (datetime != nil) ? true : false
    }

    public func getGranularity() -> String? {
        return granularity
    }

    @discardableResult public func setGranularity(value: String?) -> Self {
        granularity = value
        return self
    }

    public func hasGranularity() -> Bool {
        return (granularity != nil) ? true : false
    }

    public func getFormattedString() -> String? {
        guard let date = getDate() else {
            return nil
        }

        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = formatIn
        dateFormatterGet.locale = locale

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatOut
        dateFormatter.locale = locale

        guard let formattedDate = dateFormatterGet.date(from: date) else {
            return nil
        }

        return dateFormatter.string(from: formattedDate)
    }

    public func toDate() -> Date? {
        guard let date = getDate() else {
            return nil
        }

        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = formatIn
        dateFormatterGet.locale = locale

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatOut
        dateFormatter.locale = locale

        guard let generatedDate = dateFormatterGet.date(from: date) else {
            return nil
        }

        return generatedDate
    }
    
    public func getAgeFromDOF(date: String) -> (Int,Int,Int) {

        let dateFormater = DateFormatter()
        dateFormater.dateFormat = formatOut
        let dateOfItem = dateFormater.date(from: date)

        let calender = Calendar.current

        let dateComponent = calender.dateComponents([.year, .month, .day], from:
        dateOfItem!, to: Date())

        return (dateComponent.year!, dateComponent.month!, dateComponent.day!)
    }

}
