import Foundation

public class BaseValue: Codable {
    var value: Float?
    var unit: String?

    public init() {}

    public func getValue() -> Float? {
        return self.value
    }

    @discardableResult public func setValue(value: Float?) -> Self {
        self.value = value
        return self
    }

    public func hasValue() -> Bool {
        return (self.value != nil) ? true : false
    }

    public func getUnit() -> String? {
        return unit
    }

    @discardableResult public func setUnit(value: String?) -> Self {
        unit = value
        return self
    }

    public func hasUnit() -> Bool {
        return (unit != nil) ? true : false
    }
}
