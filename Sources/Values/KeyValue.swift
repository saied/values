import Foundation

public class KeyValue {
    var key: String
    var value: String

    public init(key: String, value: String?) {
        self.key = key
        guard let newValue = value else {
            self.value = "No Data"
            return
        }
        self.value = newValue
    }
    
    public func getKey() -> String {
        return key
    }
    
    public func getValue() -> String {
        return value
    }
}
