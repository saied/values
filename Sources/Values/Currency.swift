import Foundation

public class Currency: BaseValue {

    var currency: String?

    public func getCurrency() -> String? {
        if !hasCurrency() {
            currency = String()
        }
        return currency
    }

    @discardableResult public func setCurrency(value: String?) -> Self {
        currency = value
        return self
    }

    public func hasCurrency() -> Bool {
        return (currency != nil) ? true : false
    }

    public func getFormattedString() -> String? {
        guard let value = getValue() else {
            return nil
        }
        guard let unit = getUnit() else {
            return nil
        }
        guard let currency = getCurrency() else {
            return nil
        }

        let formatter = NumberFormatter()
        if currency != "gbp" {
            assertionFailure("currency is not pound")
            return nil
        }
        formatter.locale = Locale(identifier: "en_GB_POSIX")
        formatter.numberStyle = .currency

        if unit != "pence" {
            assertionFailure("unit is not pence")
            return nil
        }

        let toPounds = value * 0.01

        guard let formattedAmount = formatter.string(from: toPounds as NSNumber) else {
            return "No Data"
        }
        return "\(formattedAmount)"
    }
}
