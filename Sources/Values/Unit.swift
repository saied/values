import Foundation

public class Unit: BaseValue {
    
    public func getFormattedString() -> String? {
        guard let unit = getUnit() else {
            return nil
        }
        guard let value = getValue() else {
            return nil
        }

        return "\(value.clean) \(unit)"
    }

}
