import Foundation

public class Distance: BaseValue {

    public func getFormattedString() -> String? {
        guard let unit = getUnit() else {
            return nil
        }
        guard let value = getValue() else {
            return nil
        }

        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "en_GB_POSIX")
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 0
        guard let formattedAmount = formatter.string(from: value as NSNumber) else {
            return "No Data"
        }
        return "\(formattedAmount) \(unit)"
    }
}
