import Foundation

// This should probably be replaced with a suitable library
public class DateTime: Codable {
    var datetime: String?

    //Format that the API will now send dates dates associated to a record
    private let formatIn = "yyyy-MM-dd'T'HH:mm:ssZ"
    //Format for dates that don't require a time
    private let formatInOther = "yyyy-MM-dd"

    //Human readable output
    private let formatOut = "E, d MMM yyyy"

    private let locale = Locale(identifier: "en_GB_POSIX")

    public init() {}

    public func getDate() -> String? {
        return datetime
    }

    @discardableResult public func setDate(value: String?) -> Self {
        datetime = value
        return self
    }

    public func hasDate() -> Bool {
        return (datetime != nil) ? true : false
    }

    public func stringFromFullISOFormat() -> String? {
        guard let date = getDate() else {
            return nil
        }
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = formatIn
        dateFormatterGet.locale = locale

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatOut
        dateFormatter.locale = locale

        guard let formattedDate = dateFormatterGet.date(from: date) else {
            return nil
        }

        return dateFormatter.string(from: formattedDate)
    }

    public func stringFromISOFormat() -> String? {
        guard let date = getDate() else {
            return nil
        }
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = formatInOther
        dateFormatterGet.locale = locale

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatOut
        dateFormatter.locale = locale

        guard let formattedDate = dateFormatterGet.date(from: date) else {
            return nil
        }

        return dateFormatter.string(from: formattedDate)
    }

    public func stringFromConvertedFormat() -> String? {
        guard let date = getDate() else {
            return nil
        }
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = formatOut
        dateFormatterGet.locale = locale

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatInOther
        dateFormatter.locale = locale

        guard let generatedDate = dateFormatterGet.date(from: date) else {
            return nil
        }

        return dateFormatter.string(from: generatedDate)
    }

    public func toDate() -> Date? {
        guard let date = getDate() else {
            return nil
        }
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = formatIn
        dateFormatterGet.locale = locale

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatOut
        dateFormatter.locale = locale

        guard let generatedDate = dateFormatterGet.date(from: date) else {
            return nil
        }

        return generatedDate
    }

    public func fromConvertedStringToDate() -> Date? {
        guard let date = getDate() else {
            return nil
        }

        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = formatOut
        dateFormatterGet.locale = locale

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatOut
        dateFormatter.locale = locale

        guard let generatedDate = dateFormatterGet.date(from: date) else {
            return nil
        }

        return generatedDate
    }

}
