import XCTest
@testable import Values

final class ValuesTests: XCTestCase {
    func testUnit() {
        var unit = Unit()
        unit.setValue(value: 1.0).setUnit(value: "test")
        XCTAssertEqual(unit.getFormattedString(), "1 test")

        unit = Unit()
        unit.setValue(value: 2).setUnit(value: "another test")
        XCTAssertEqual(unit.getFormattedString(), "2 another test")

        unit = Unit()
        unit.setValue(value: 3)
        XCTAssertNil(unit.getFormattedString())

        unit = Unit()
        unit.setUnit(value: "test")
        XCTAssertNil(unit.getFormattedString())
    }
    func testGranularDateTime() {
        var granularDateTime = GranularDateTime()
        granularDateTime.setDate(value: "Thu, 01 Nov 2018 00:00:00 +0000").setGranularity(value: "Y-m-d")
        XCTAssertEqual(granularDateTime.getFormattedString(), "Thu, 1 Nov 2018")

        granularDateTime = GranularDateTime()
        granularDateTime.setDate(value: "Thu, 01 Nov 2018 00:00:00 +0000").setGranularity(value: "Y-m-d hh:mm:ss")
        XCTAssertEqual(granularDateTime.getFormattedString(), "Thu, 1 Nov 2018")

        granularDateTime = GranularDateTime()
        granularDateTime.setDate(value: "Thu, 01 Nov 2018 00:00:00 +0000")
        XCTAssertEqual(granularDateTime.getFormattedString(), "Thu, 1 Nov 2018")

        granularDateTime = GranularDateTime()
        granularDateTime.setGranularity(value: "Y-m-d hh:mm:ss")
        XCTAssertNil(granularDateTime.getFormattedString())

        granularDateTime = GranularDateTime()
        granularDateTime.setDate(value: "Thu, 01 Nov 2018 00:00:00 +0000")
        XCTAssertNotNil(granularDateTime.toDate())

    }
    func testDistance() {
        var distance = Distance()
        distance.setValue(value: 1.0).setUnit(value: "mi")
        XCTAssertEqual(distance.getFormattedString(), "1 mi")

        distance = Distance()
        distance.setValue(value: 2).setUnit(value: "km")
        XCTAssertEqual(distance.getFormattedString(), "2 km")

        distance = Distance()
        distance.setValue(value: 3)
        XCTAssertNil(distance.getFormattedString())

        distance = Distance()
        distance.setUnit(value: "mi")
        XCTAssertNil(distance.getFormattedString())
    }
    func testDateTime() {
        var dateTime = DateTime()
        dateTime.setDate(value: "2019-09-03T16:38:18Z")
        XCTAssertEqual(dateTime.stringFromFullISOFormat(), "Tue, 3 Sep 2019")

        dateTime = DateTime()
        dateTime.setDate(value: "2019-09-03")
        XCTAssertEqual(dateTime.stringFromISOFormat(), "Tue, 3 Sep 2019")

        dateTime = DateTime()
        dateTime.setDate(value: "Tue, 3 Sep 2019")
        XCTAssertEqual(dateTime.stringFromConvertedFormat(), "2019-09-03")

        dateTime = DateTime()
        dateTime.setDate(value: "Tue, 3 Sep 2019")
        XCTAssertNotNil(dateTime.fromConvertedStringToDate())

        dateTime = DateTime()
        XCTAssertNil(dateTime.stringFromISOFormat())

        dateTime = DateTime()
        dateTime.setDate(value: "2019-09-03T16:38:18Z")
        XCTAssertNotNil(dateTime.toDate())
    }
    func testCurrency() {
        var currency = Currency()
        currency.setValue(value: 9999).setUnit(value: "pence").setCurrency(value: "gbp")
        XCTAssertEqual(currency.getFormattedString(), "£99.99")

        currency = Currency()
        currency.setValue(value: 999).setUnit(value: "pence").setCurrency(value: "gbp")
        XCTAssertEqual(currency.getFormattedString(), "£9.99")

        currency = Currency()
        currency.setValue(value: 2)
        XCTAssertNil(currency.getFormattedString())

        currency = Currency()
        currency.setUnit(value: "pence")
        XCTAssertNil(currency.getFormattedString())

        currency = Currency()
        currency.setCurrency(value: "gbp")
        XCTAssertNil(currency.getFormattedString())
    }
    func testKeyValue() {
        var keyValue = KeyValue(key: "Key", value: "Value")
        XCTAssertEqual(keyValue.key, "Key")
        XCTAssertEqual(keyValue.value, "Value")

        keyValue = KeyValue(key: "Key", value: nil)
        XCTAssertEqual(keyValue.key, "Key")
        XCTAssertEqual(keyValue.value, "No Data")

    }

    static var allTests = [
        ("Unit test", testUnit)
    ]
}
