import XCTest

import ValuesTests

var tests = [XCTestCaseEntry]()
tests += ValuesTests.allTests()
XCTMain(tests)
