// swift-tools-version:5.1

import PackageDescription

let package = Package(
    name: "Values",
    products: [

        .library(
            name: "Values",
            targets: ["Values"])
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [

        .target(
            name: "Values",
            dependencies: []),
        .testTarget(
            name: "ValuesTests",
            dependencies: ["Values"])
    ]
)
